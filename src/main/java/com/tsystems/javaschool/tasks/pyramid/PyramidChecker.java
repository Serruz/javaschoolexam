package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidChecker {
   private int i=1;
    private List<Integer> input;
    public PyramidChecker(List<Integer> inputNumbers)
    {
        this.input=inputNumbers;
    }

    protected int getNumberOrder() {
        return i;
    }

    protected boolean checkList()
    {
        long s=0;
        boolean check=false;
        if(!input.contains(null)) {
            while (s < input.size()) {
                s += i;
                if (s == input.size())
                    check = true;
                else if (s >= input.size())
                    check = false;
                else
                    i++;

            }
        }
        return check;
    }
}
