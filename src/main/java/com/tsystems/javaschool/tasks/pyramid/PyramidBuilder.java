package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        PyramidChecker checker = new PyramidChecker(inputNumbers);
        int[][] result;
        if (checker.checkList()) {
            result = new int[checker.getNumberOrder()][2 * checker.getNumberOrder() - 1];
            Collections.sort(inputNumbers);
            int curIndex = 0;
            int mid = (2 * checker.getNumberOrder() - 1) / 2;
            for (int column = 0; column < result.length; column++) {
                ArrayList<Integer> currElements = new ArrayList<>();
                for (int i = 0; i <= column; i++) {
                    currElements.add(inputNumbers.get(curIndex + i));
                }
                int padding = currElements.size() - 1;
                int pos = mid - padding;
                for (int n : currElements) {
                    result[column][pos] = n;
                    pos += 2;
                }
                curIndex += currElements.size();
            }
        } else
            throw new CannotBuildPyramidException();
        return result;
    }


}
