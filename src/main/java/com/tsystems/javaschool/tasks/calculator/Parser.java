package com.tsystems.javaschool.tasks.calculator;

public class Parser {
    private int pos = -1;
    private int ch;
    String str;

    public Parser(String statement) {
        this.str = statement;
    }

    public double eval() {
        int count1 = 0;
        int count2 = 0;
        for (char s : str.toCharArray()) {
            if (s == '(')
                count1++;
            else if (s == ')')
                count2++;
        }
        if (count2 != count1)
            throw new RuntimeException("Brackets do not match");
        double result = parse(str);
        if (Double.isInfinite(result))
            throw new RuntimeException("Infinity");
        return result;
    }

    private double parse(String str) {
        nextChar(str);
        double x = parseExpression(str);
        if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char) ch);
        return x;
    }

    private void nextChar(String str) {
        ch = (++pos < str.length()) ? str.charAt(pos) : -1;
    }

    private double parseExpression(String str) {
        double x = parseOperation(str);
        while (true) {
            if (handle('+', str)) x += parseOperation(str);
            else if (handle('-', str)) x -= parseOperation(str);
            else return x;
        }
    }

    private double parseElement(String str) {
        double x;
        int startPos = pos;
        if (handle('(', str)) {
            x = parseExpression(str);
            handle(')', str);
        } else if ((ch >= '0' && ch <= '9') || ch == '.') {
            while ((ch >= '0' && ch <= '9') || ch == '.') nextChar(str);
            x = Double.parseDouble(str.substring(startPos, pos));
        } else {
            throw new RuntimeException("Unexpected: " + (char) ch);
        }
        return x;
    }

    private double parseOperation(String str) {
        double x = parseElement(str);
        while (true) {
            if (handle('*', str)) x *= parseElement(str);
            else if (handle('/', str)) x /= parseElement(str);
            else return x;
        }
    }

    private boolean handle(int elem, String str) {
        while (ch == ' ') nextChar(str);
        if (ch == elem) {
            nextChar(str);
            return true;
        }
        return false;
    }
}
