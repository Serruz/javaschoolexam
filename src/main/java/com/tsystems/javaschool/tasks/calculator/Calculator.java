package com.tsystems.javaschool.tasks.calculator;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        try {
            Parser parser = new Parser(statement);
            Double check = parser.eval();
            if (check % 1 == 0) {
                return Integer.toString(check.intValue());
            } else {

                return Double.toString(check);
            }
        } catch (Exception e) {
            return null;
        }
    }


}
